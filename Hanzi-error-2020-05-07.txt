Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "之","definition": "marks preceding phrase as modifier of following phrase; it, him her, them; to go to","radical": "丿","pinyin": ["zhī"],"decomposition": "⿱丶？","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "A foot meaning "to follow"; cursive version of 止"},"matches": [[0], [], []]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "仌","definition": "frozen; ice-cold","radical": "人","pinyin": ["bīng"],"decomposition": "⿱人人","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "Old variant of 冰 ("ice-water")"},"matches": [[0], [0], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "他","definition": "other, another; he, she, it","radical": "亻","pinyin": ["tā"],"decomposition": "⿰亻也","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "An additional, "also" 也  person 亻"},"matches": [[0], [0], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "兜","definition": "pouch","radical": "儿","pinyin": ["dōu"],"decomposition": "⿱⿴？白儿","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "A man wearing a helmet, now meaning "pouch""},"matches": [[], [], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [], [], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "入","definition": "to enter, to come in; to join","radical": "入","pinyin": ["rù"],"decomposition": "？","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "An arrow indicating "enter""},"matches": [[], []]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "八","definition": "eight; all around, all sides","radical": "八","pinyin": ["bā"],"decomposition": "？","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "Two bent lines meaning "to divide""},"matches": [[], []]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "北","definition": "north; northern; northward","radical": "匕","pinyin": ["běi"],"decomposition": "⿰？匕","etymology": {"type": "pictographic", "phonetic": "", "semantic": "", "hint": "Two people 匕 sitting back-to-back; phonetic loan for "north""},"matches": [[], [], [], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "幹","definition": "arid, dry; to oppose; to offend; to invade","radical": "干","pinyin": ["gàn"],"decomposition": "⿰龺⿱人干","etymology": {"type": "pictographic", "phonetic": "", "semantic": "", "hint": "Sunlight 龺 suggests "dry"; a club 人干 suggests "invade""},"matches": [[0], [0], [0], [0], [0], [0], [0], [0], [1, 0], [1, 0], [1, 1], [1, 1], [1, 1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "幻","definition": "fantasy, illusion, mirage; imaginary","radical": "幺","pinyin": ["huàn"],"decomposition": "⿰幺？","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "An inversion of 予, "to give""},"matches": [[0], [0], [0], []]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "或","definition": "or, either, else; maybe, perhaps, possibly","radical": "戈","pinyin": ["huò"],"decomposition": "⿹戈⿱口一","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "To defend yourself 口 with a spear 戈; "or else""},"matches": [[0], [1, 0], [1, 0], [1, 0], [1, 1], [0], [0], [0]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "所","definition": "place, location; "that which", a particle introducing a passive clause","radical": "户","pinyin": ["suǒ"],"decomposition": "⿰户斤","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "An axe 斤 swung at a door 户"},"matches": [[0], [0], [0], [0], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "昝","definition": "dual pronoun: "you and I", "we two"; surname","radical": "日","pinyin": ["zǎn"],"decomposition": "⿱处日","etymology": {"type": "", "phonetic": "", "semantic": "", "hint": ""},"matches": [[0], [0], [0], [0], [0], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "朕","definition": "the royal "we", for imperial use","radical": "月","pinyin": ["zhèn"],"decomposition": "⿰月关","etymology": {"type": "", "phonetic": "", "semantic": "", "hint": ""},"matches": [[0], [0], [0], [0], [1], [1], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "瓣","definition": "petal; segment; valve","radical": "瓜","pinyin": ["bàn"],"decomposition": "⿲辛瓜辛","etymology": {"type": "pictophonetic", "phonetic": "







辡", "semantic": "瓜", "hint": "melon"},"matches": [[0], [0], [0], [0], [0], [0], [0], [1], [1], [1], [1], [1], [2], [2], [2], [2], [2], [2], [2]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "甩","definition": "to throw away, to fling, to discard","radical": "用","pinyin": ["shuǎi"],"decomposition": "⿻月乚","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "The opposite of "to use" 用"},"matches": [[0], [0], [0], [0], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "甭","definition": ""there is no need"","radical": "用","pinyin": ["béng"],"decomposition": "⿱不用","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": "Not 不 needed 用; 用 also provides the pronunciation"},"matches": [[0], [0], [0], [0], [1], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "肖","definition": "to resemble, to look like; to be like","radical": "⺼","pinyin": ["xiào"],"decomposition": "⿱⺌⺼","etymology": {"type": "pictophonetic", "phonetic": "⺌", "semantic": "⺼", "hint": "flesh - "in the flesh""},"matches": [[0], [0], [0], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "被","definition": "bedding; a passive particle meaning "by"","radical": "衤","pinyin": ["bèi"],"decomposition": "⿰衤皮","etymology": {"type": "pictophonetic", "phonetic": "皮", "semantic": "衤", "hint": "cloth"},"matches": [[0], [0], [0], [0], [0], [1], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "贗","definition": "false, bogus; counterfeit; a sham","radical": "貝","pinyin": ["yàn"],"decomposition": "⿸鴈貝","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": ""Wild goose" 鴈 money 貝; 鴈 also provides the pronunciation"},"matches": [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1], [1], [1], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "赝","definition": "false, bogus; counterfeit; a sham","radical": "贝","pinyin": ["yàn"],"decomposition": "⿸雁贝","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": ""Wild goose" 雁 money 贝; 雁 also provides the pronunciation"},"matches": [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1], [1], [1], [1]]}


Hanzi error: The data couldn’t be read because it isn’t in the correct format.
JSON: { "character": "贗","definition": "false, bogus; counterfeit; a sham","radical": "貝","pinyin": ["yàn"],"decomposition": "⿸鴈貝","etymology": {"type": "ideographic", "phonetic": "", "semantic": "", "hint": ""Wild goose" 鴈 money 貝; 鴈 also provides the pronunciation"},"matches": [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1], [1], [1], [1], [1], [1], [1]]}